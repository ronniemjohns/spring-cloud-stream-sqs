terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "4.1.0"
    }
  }
}

provider "aws" {
  region                      = "us-east-1"
  access_key                  = "test123"
  secret_key                  = "testabc"
  skip_credentials_validation = true
  skip_requesting_account_id  = true
  skip_metadata_api_check     = true
  endpoints {
    sqs = "http://localhost:4566"
  }
}

# Create SQS
resource "aws_sqs_queue" "inQueue" {
  name      =   "inQueue"
}
resource "aws_sqs_queue" "outQueue" {
  name      =   "outQueue"
}