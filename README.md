# SQS using Cloud Stream Bindings
## General Description

Using Spring Cloud Stream and bindings for SQS allows you to use configuration to decouple the message process from the 
Amazon specific logic.  In this way, you could configure Amazon queues, Azure queues and RabbitMQ queues to all be 
picked up & processed by the same message processor file. 

---

## Details

For Amazon SQS, a binder is imported (see build.gradle) and then configured through application.properties (or a yaml file).
Those properties define the name of the METHODS that will send and receive messages as well as the QUEUES that are bound
to the methods.  In this demo, the methods are simply "send" and "receive".  See their bean definitions in the 
Applications class.  

A different binder (Azure, Kafka, RabbitMQ) could also use those same methods for binding.

Note the MessageProcessingService/Impl is where all of the business logic is located.

---
## Setup & Testing
Make sure localstack is running, and then apply the terraform plan under infra/mod_localstack (terraform apply).

Once the queues are up and running locally, startup this application.

Once started, you can send a message using something like:
~~~
   aws --endpoint-url=http://localhost:4566 sqs send-message --queue-url=http://localhost:4566/000000000000/inQueue --message-body "HelloWorld"
~~~

You should see a resulting output such as:
~~~
2022-03-14 11:10:22.287  INFO 37609 --- [enerContainer-2] c.e.d.s.SpringCloudStreamSqsApplication  : Here's the payload : HelloWorld
2022-03-14 11:10:22.288  INFO 37609 --- [enerContainer-2] c.e.d.s.s.MessageProcessingServiceImpl   : Processing message : HelloWorld
~~~
---

## Links

[Main Cloud Stream Project Page](https://spring.io/projects/spring-cloud-stream)

[Spring Cloud Steram Binder for Amazon SQS](https://github.com/idealo/spring-cloud-stream-binder-sqs)

[AWS CLI commands when using localstack](https://lobster1234.github.io/2017/04/05/working-with-localstack-command-line/)

