package com.example.demo.springcloudstreamsqs;

import com.example.demo.springcloudstreamsqs.dto.Message;
import com.example.demo.springcloudstreamsqs.service.MessageProcessingServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.function.Consumer;
import java.util.function.Supplier;

@SpringBootApplication
public class Application {

    private static Logger LOG = LoggerFactory.getLogger(Application.class);

    @Autowired
    private MessageProcessingServiceImpl messageProcessor;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public Supplier<Message> send() {
        return messageProcessor.sendMessage();
    }

    @Bean
    public Consumer<String> receive() {
        return payload -> {
            LOG.info("Here's the payload : {}", payload);
            messageProcessor.processMessage(payload);
        };
    }



}
