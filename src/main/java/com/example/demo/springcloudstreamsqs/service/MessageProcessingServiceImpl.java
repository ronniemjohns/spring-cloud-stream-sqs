package com.example.demo.springcloudstreamsqs.service;

import com.example.demo.springcloudstreamsqs.dto.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.function.Supplier;

@Service
public class MessageProcessingServiceImpl implements MessageProcessingService {

    private static Logger LOG = LoggerFactory.getLogger(MessageProcessingServiceImpl.class);

    @Override
    public void processMessage(String message) {
        LOG.info("Processing message : {}", message);
    }

    @Override
    public Supplier<Message> sendMessage() {
        LOG.info("Sending message");
        return () -> new Message("New Message here");
    }
}
