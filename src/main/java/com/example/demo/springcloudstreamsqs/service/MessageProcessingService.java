package com.example.demo.springcloudstreamsqs.service;

import com.example.demo.springcloudstreamsqs.dto.Message;

import java.util.function.Supplier;

public interface MessageProcessingService {

    void processMessage(String message);

    Supplier<Message> sendMessage();
}
